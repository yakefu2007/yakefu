---
home: true
layout: BlogHome
icon: home
title: Home
bgImage: https://hbwg.hestudio.net/getimage
# heroImage: /logo.svg
heroText: yakefu
tagline: 
heroFullScreen: true
projects:
  - icon: https://www.hestudio.net/quickref.svg
    name: Quick Reference
    desc: 为开发人员分享快速参考备忘清单
    link: https://quickref.hestudio.net/
  - icon: /assets/python.svg
    name: Python
    desc: Welcome to Python.org
    link: https://python.org
  - icon: api
    name: 百度搜索提交工具
    desc: 基于Python的快速提交网页到百度的接口
    link: https://pypi.org/project/hbsst/
  - icon: token
    name: heStudio MFA for Desktop
    desc: 一个用Java写的的桌面端MFA应用
    link: https://github.com/hestudio-community/hmfa

# footer: customize your footer text
---

