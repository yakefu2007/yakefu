import { defineUserConfig } from "vuepress";
import { searchPlugin } from "@vuepress/plugin-search";
import { removePWAPlugin } from "vuepress-plugin-remove-pwa";
import theme from "./theme.js";


export default defineUserConfig({
  base: "/",

  locales: {
    "/": {
      lang: "zh-CN",
      title: "yakefu",
      description: "",
    },
  },

  theme,


  plugins: [
    // socialSharePlugin({
    //   networks: ["email", "weibo", "douban", "qq", "wechat", "qrcode"],
    // }),
    removePWAPlugin({
      cachePrefix: "workbox",
      swLocation: 'service-worker.js',
    }),
    searchPlugin({
      // 配置项
      maxSuggestions: 100,
    }),
  ],

  shouldPrefetch: false,
});
